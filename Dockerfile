# Include base images
FROM rust:1.26

# Tunables and reusables
ARG APP_USER=docker-app 
ARG APP_GROUP=docker-app 
ARG APP_USER_HOME=/app 
ARG APP_HOME=/app/run

# Set up the user home
RUN mkdir -p $APP_USER_HOME && mkdir -p $APP_HOME
RUN groupadd -r $APP_GROUP && \
    useradd -r -g $APP_GROUP -d $APP_USER_HOME -s /bin/nologin -c "Docker user" $APP_USER
WORKDIR $APP_HOME

# expose the endpoints
EXPOSE 8080

# Copy app data
COPY . $APP_HOME
RUN chown -R $APP_USER:$APP_GROUP $APP_HOME

# Change to the user
USER $APP_USER:$APP_GROUP

# Install app dependencies
RUN cargo install

# Run the app
CMD ["drones"]
