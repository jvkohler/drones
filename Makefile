docker-run: docker-build
	@docker run -P -it --rm=true --name drones-server drones-image

docker-build:
	@docker build -t drones-image .

check:
	@cargo clean -p drones
	@cargo check

build:
	@cargo build -v 

lint:
	@cargo +nightly clean -p drones
	@cargo +nightly clippy

test:
	@cargo test -v

run:
	@cargo run -v

docs:
	@cargo doc --no-deps
