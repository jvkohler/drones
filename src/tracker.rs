//! # The drone related module
//!
//! ## How to use
//!
//! First, create mutable DroneTrackerMem instance.
//!
//!     let mut tracker: DroneTrackerMem = DroneTrackerMem::new( );
//!
//! Create a Drone instance.
//!
//!     let drone = Drone::new( 1, Position::new( 1.0, 1.0, 1.0 ) );
//!
//! Add it to the tracker.
//!
//!     tracker.update( drone );
//!
//! If you have a buffer of packetized drone data, you can pass it Drone::new_from_buffer.
//!
//!     let socket = UdpSocket::bind("127.0.0.1:34254")?;
//!     let mut buf = [0; 17];
//!     let (amt, src) = socket.recv_from(&mut buf)?;
//!     Drone::new_from_buffer( &buf )
//!
//! ## JSON representation
//!
//! Each Drone will have the following structure in HTTP results and also as a result of calling Drone::to_json,
//! with fields described below.
//!
//!     {
//!         "uuid": <string>,
//!         "last_moved": <string>,
//!         "last_updated": <string>,
//!         "position": {
//!             "latitude": <float>,
//!             "longitude": <float>,
//!             "altitude": <float>
//!         }
//!     }
//!
//! ### Field Descriptions
//!
//! `uuid`: This is an opaque UUID for the Drone. It is guaranteed that two strings that are equal refer to the same two drones
//! but otherwise there is no meaning that should be interpreted or ascribed to this field.
//!
//! `last_moved`: This is a string containing a date and time in RFC3339 format, which is ISO 8601 compliant. This is always in
//! UTC time. This is the last time that movement was detected from the Drone.
//!
//! `last_updated`: This is a string containing a date and time in RFC3339 format, which is ISO 8601 compliant. This is always
//! in UTC time. This is the last time that the Drone contacted the server, whether or not movement was detected.
//!
//! `position': This structure contains the 3 dimensional position of the Drone.
//!
//! `position.latitude`: This is a floating point number guaranteed to have six digits of precision. This represents the current
//! latitudinal position of the Drone in degrees with fractional components represented in decimal.
//!
//! `position.longitude`: This is a floating point number guaranteed to have six digits of precision. This represents the current
//! longitudinal position of the Drone in degrees with fractional components represented in decimal.
//!
//! `position.altitude`: This is a floating point number guaranteed to have two digits of precision. This represents the current
//! altitude of the Drone in feet above or below sea level.
//!
//! ## Limitations
//!
//! At this time there is no way to determine if a particular UUID is in use, or to ask for an unused one.
//!
//! UUIDs are plain u32s right now, and there's no way to choose a different type. This will be changed in the future.
//!
//! There is no way to choose a DroneTracker implementation without hard coding it. This will be changed in the future.
//!
//! A floating point tolerance of 6 decimal places is hard coded. This will be adjustable in the future.
//!

extern crate std;
extern crate byteorder;
extern crate chrono;

use std::sync::{ Arc, RwLock };

use tracker::byteorder::{ByteOrder, BigEndian};

use tracker::chrono::offset::Utc;
use tracker::chrono::{ DateTime, Duration };

#[ cfg( test ) ]
mod tests {
	use super::*;

	#[ test ]
	fn drones_test_position( ) {
		// Test data
		let a: Position = Position::new( 1.0, 1.0, 1.0 );
		let b: Position = Position::new( 1.0, 1.0, 1.0 );
		let c: Position = Position::new( 2.0, 2.0, 2.0 );

		// Equality testing
		assert_eq!( a.lat, 1.0 );
		assert_eq!( c.alt, 2.0 );
		assert_eq!( a, b );
		assert_ne!( b, c );

		// Pass invalid values
		let result = std::panic::catch_unwind( || Position::new( std::f64::NAN, 1.0, 1.0 ) );
		assert!( result.is_err( ) );
	}

	#[ test ]
	fn drones_test_drone_state( ) {
		// Test data
		let mut a: DroneState = DroneState::new( Position::new( 1.0, 1.0, 1.0 ) );
		let b: DroneState = DroneState::new( Position::new( 1.0, 1.0, 1.0 ) );
		let c: DroneState = DroneState::new( Position::new( 2.0, 2.0, 2.0 ) );

		// Equality testing
		assert_ne!( a, b );
		assert_ne!( b, c );
		assert_eq!( a, a );
		assert_eq!( a.get_position( ), b.get_position( ) );

		// Verify non-movement update
		let mut t = a.last_update;
		a.set_position( b.get_position( ) );
		assert_ne!( a.last_update, t );
		assert_ne!( a.last_update, a.last_movement );

		// Verify movement update
		t = a.last_update;
		a.set_position( c.get_position( ) );
		assert_ne!( a.last_update, t );
		assert_eq!( a.last_update, a.last_movement );
		assert_ne!( a.get_position( ), b.get_position( ) );
		assert_eq!( a.get_position( ), c.get_position( ) );

		// Verify ordering
		assert!( a > c );
		assert!( b < c );
	}

	#[ test ]
	fn drones_test_drone( ) {
		// Test data
		let mut a: Drone = Drone::new( 1, Position::new( 1.0, 1.0, 1.0 ) );
		let b: Drone = Drone::new( 1, Position::new( 2.0, 2.0, 2.0 ) );
		let c: Drone = Drone::new( 2, Position::new( 3.0, 3.0, 3.0 ) );

		// Equality testing
		assert_eq!( a.get_uuid( ), 1 );
		assert_eq!( a, b );
		assert_ne!( b, c );

		let t = a.get_last_update( );

		// Test passthrough get/set.
		assert_ne!( a.get_position( ), b.get_position( ) );
		a.set_position( b.get_position( ) );
		assert_eq!( a.get_position( ), b.get_position( ) );

		// Verify passthrough, relying on the appropriate behaviour of DroneState
		assert_ne!( a.get_last_update( ), t );
		assert_eq!( a.get_last_movement( ), a.get_last_update( ) );

		// Test creation from DroneState
		let d: Drone = Drone::new_from_state( 1, a.state );
		assert_eq!( a, d );
	}

	#[ test ]
	fn drones_test_tracker_mem( ) {
		// Test data
		let mut t: DroneTrackerMem = DroneTrackerMem::new( );
		let mut a = Drone::new( 1, Position::new( 1.0, 1.0, 1.0 ) );
		let b = Drone::new( 2, Position::new( 2.0, 2.0, 2.0 ) );

		// verify empty list.
		assert_eq!( t.len( ), 0 );

		// Add the first drone and update it to the same values.
		t.update( a );
		t.update( a );

		// Add the second drone.
		t.update( b );

		// Verify drone list contains both drones.
		assert_eq!( t.len( ), 2 );
		assert_eq!( t.list( ), [ Box::new( a ), Box::new( b ) ] );

		// Update the first drone to new values.
		a.set_position( Position::new( 2.0, 3.0, 4.0 ) );
		t.update( a );

		// Verify that we didn't create a new one.
		assert_eq!( t.len( ), 2 );

		// remove the second drone and verify the list
		t.remove( b.get_uuid( ) );
		assert_eq!( t.len( ), 1 );
		assert_eq!( t.list( ), [ Box::new( a ) ] );

		// Remove the second drone again.
		t.remove( b.get_uuid( ) );

		// Remove the first drone and verfify the list is empty.
		t.remove( a.get_uuid( ) );
		assert_eq!( t.len( ), 0 );

		// Verify finding drones that haven't moved
		// We'll use the example data.
		// UUID 1 and 3 have not moved recently, 2 and 4 have.
		assert_eq!( t.get_unmoved( 10 ).len( ), 0 );
		t.load_example_data( );
		assert_eq!( t.list( ).len( ), 4 );
		assert_eq!( t.get_unmoved( 100 ).len( ), 2 );

		// Verify the order
		assert_eq!( t.get_unmoved( 100 )[ 0 ].get_uuid( ), 3 );
		assert_eq!( t.get_unmoved( 100 )[ 1 ].get_uuid( ), 1 );
		assert_eq!( t.list( )[ 0 ].get_uuid( ), 1 );
		assert_eq!( t.list( )[ 1 ].get_uuid( ), 2 );
		assert_eq!( t.list( )[ 2 ].get_uuid( ), 3 );
		assert_eq!( t.list( )[ 3 ].get_uuid( ), 4 );

		// Make the oldest move and reverify
		t.update( Drone::new( 3, Position::new( 5.0, 5.0, 5.0 ) ) );
		assert_eq!( t.get_unmoved( 100 ).len( ), 1 );
		assert_eq!( t.get_unmoved( 100 )[ 0 ].get_uuid( ), 1 );
		assert_eq!( t.list( )[ 0 ].get_uuid( ), 1 );
		assert_eq!( t.list( )[ 1 ].get_uuid( ), 2 );
		assert_eq!( t.list( )[ 2 ].get_uuid( ), 3 );
		assert_eq!( t.list( )[ 3 ].get_uuid( ), 4 );
	}

	#[ test ]
	fn drones_test_tracker_mem_threadsafety( ) {
		static NTHREADS: i32 = 10;
		let t: DroneTrackerMem = DroneTrackerMem::new( );
		let mut handles = vec![ ];

		// Add a drone on each thread
		for i in 0..NTHREADS {
			// Copy variables to move into the thread
			// This masks ours at the same time.
			let i = i;
			let mut t = t.duplicate( );

			// Create the new thread, and store the thread handle
			handles.push(
				std::thread::spawn( move || {
					let d = Drone::new( i as u32, Position::new( i as f64, i as f64, i as f64 ) );
					t.update( d );
				} )
			);
		}

		// Wait for all threads to finish
		for child in handles {
			// Unwrap to force a panic to propogate.
			child.join( ).unwrap( );
		}

		// Verify count
		assert_eq!( t.len( ), 10 );
	}

	#[ test ]
	fn drones_test_drone_from_buffer ( ) {
		// This buffer is a drone with UUID 305419896, latitude 1.23, longitude 4.56, altitude 7.89.
		let buf = [ 0x01,
			0x12, 0x34, 0x56, 0x78,
			0x3f, 0x9d, 0x70, 0xa4,
			0x40, 0x91, 0xeb, 0x85,
			0x40, 0xfc, 0x7a, 0xe1
			];
		let d = Drone::new_from_buffer( &buf );
		assert_eq!( d.get_uuid( ), 305419896 );
		assert_eq!( d.get_position( ), Position::new( 1.23, 4.56, 7.89 ) );
	}
}

/// Position represents a point in three dimensional space.
///
/// Altitude is in feet, latitude and longitude are in decimal degrees.
///
/// You can safely compare these for equality. The floating point nubmers are compared to six places of precision.
/// If all fields are equal to that precision, then the objects are considered equal.
#[ derive( Debug, Clone, Copy ) ]
pub struct Position {
	lat:  f64,
	lon:  f64,
	alt:  f64,
}

// TODO: Pass an Epsilon value in constructor to allow tunable tolerance.
impl Position {
	/// Return a new Position object.
	pub fn new( lat: f64, lon: f64, alt: f64 ) -> Position {
		if ! ( lat.is_finite( ) && lon.is_finite( ) && alt.is_finite( ) ) {
			panic!( "Positions must not be infinite or NaN" );
		}
		Position { lat, lon, alt }
	}

	/// Return the latitude in decimal as a float.
	pub fn get_lat( &self ) -> f64 {
		self.lat
	}

	/// Return the longitude in decimal as a float.
	pub fn get_lon( &self ) -> f64 {
		self.lon
	}

	/// Return the altitude in feet as a float.
	pub fn get_alt( &self ) -> f64 {
		self.alt
	}
}

/// Two Positions are equal if the differenc of each pair of components is equivalent to 6 decimal places.
impl PartialEq for Position {
	fn eq( &self, rhs: &Position ) -> bool {
		( ( self.get_lat( ) - rhs.get_lat( ) ).abs( ) < 1.0e-6 ) &&
		( ( self.get_lon( ) - rhs.get_lon( ) ).abs( ) < 1.0e-6 ) &&
		( ( self.get_alt( ) - rhs.get_alt( ) ).abs( ) < 1.0e-6 )
	}
}

// The floats prevent deriving this, but we gaurantee that we're not dealing with NaNs so we can tag it ourselves.
impl Eq for Position {
}

// This is an internal only structure solely for storing state in a map at this time.
// TODO: This is probably irrelevant and unnecessary now that we're using vectors instead of maps. Reevaluate.
#[ derive( Debug, Clone, Copy, PartialEq, Eq ) ]
struct DroneState {
	pos: Position,		// The current position
	last_update: DateTime<Utc>,	// the last time this drone was updated
	last_movement: DateTime<Utc>,	// the last time movement was detected
}

impl DroneState {
	// Create a new DroneState object
	#[cfg_attr( feature = "cargo-clippy", allow( redundant_field_names ) ) ]
	pub fn new( pos: Position ) -> DroneState {
		let now = Utc::now( );

		DroneState {
			pos: pos,
			last_update: now,
			last_movement: now,
		}
	}


	// Update the position of a drone object
	pub fn set_position( &mut self, p: Position ) {
		let now = Utc::now( );

		if self.pos != p {
			self.last_movement = now;
		}
		self.last_update = now;
		self.pos = p;
	}

	// Return the last known position as a Position object
	pub fn get_position( &self ) -> Position {
		self.pos
	}
}

// PartialOrd is necessary for Ord
// We defer to the Ord implementation, as suggested in the Rust documentation.
impl PartialOrd for DroneState {
	fn partial_cmp ( &self, rhs: &DroneState ) -> Option< std::cmp::Ordering > {
		Some( self.cmp( rhs ) )
	}
}

// Ord is necessary for using in a BTreeMap
// We compare the last movement time of the two DroneStates.
impl Ord for DroneState {
	fn cmp ( &self, rhs: &DroneState ) -> std::cmp::Ordering {
		self.last_movement.cmp( &rhs.last_movement )
	}
}

/// Public Drone class, used to pass data to and from the manager.
/// TODO: The UUID should be an impl that has Traits PartialEq PartialOrd
#[ derive( Debug, Clone, Copy, Eq ) ]
pub struct Drone {
	uuid: u32, 		// The UUID
	state: DroneState,
}

impl Drone {
	/// Create a new Drone object
	#[ cfg_attr( feature = "cargo-clippy", allow( redundant_field_names ) ) ]
	pub fn new( uuid: u32, pos: Position ) -> Drone {
		let now = Utc::now( );

		Drone {
			uuid: uuid,
			state: DroneState {
				pos: pos,
				last_update: now,
				last_movement: now,
			}
		}
	}

	// Private function for create with an existing DroneState
	fn new_from_state( uuid: u32, state: DroneState ) -> Drone {
		Drone { uuid, state }
	}

	/// Decode a network buffer and return a new Drone
	pub fn new_from_buffer( buf: &[ u8 ] ) -> Drone {
		// Verify packet version
		let version: u8 = buf[ 0 ];
		assert_eq!( version, 1 );

		// verify buffer size
		assert_eq!( buf.len( ), 17 );

		// Decode
		let uuid: u32 = BigEndian::read_u32( &buf[ 1..5 ] );
		let lat: f32 = BigEndian::read_f32( &buf[ 5..9 ] );
		let lon: f32 = BigEndian::read_f32( &buf[ 9..13 ] );
		let alt: f32 = BigEndian::read_f32( &buf[ 13..17 ] );

		// Create the Drone and return it
		Drone::new( uuid, Position::new( lat as f64, lon as f64, alt as f64 ) )
	}

	/// Return the UUID of this object
	pub fn get_uuid( &self ) -> u32 {
		self.uuid
	}

	/// Update the position of a drone object
	pub fn set_position( &mut self, p: Position ) {
		self.state.set_position( p )
	}

	/// Return the last known position as a Position object
	pub fn get_position( &self ) -> Position {
		self.state.get_position( )
	}

	/// Return the last time the drone was updated, whether it moved or not
	pub fn get_last_update( &self ) -> DateTime<Utc> {
		self.state.last_update
	}

	/// Return the last time the drone moved
	pub fn get_last_movement( &self ) -> DateTime<Utc> {
		self.state.last_movement
	}

	/// Return the Drone as a JSON object
	pub fn to_json( &self ) -> String {
		format!(
"{{
	\"uuid\" : \"{}\",
	\"last_moved\" : \"{}\",
	\"last_updated\" : \"{}\",
	\"position\" : {{
		\"latitude\" : {:1.6},
		\"longitude\" : {:1.6},
		\"altitude\" : {:1.2}
	}}
}}",
		self.get_uuid( ),
		self.get_last_movement( ).to_rfc3339( ),
		self.get_last_update( ).to_rfc3339( ),
		self.get_position( ).get_lat( ),
		self.get_position( ).get_lon( ),
		self.get_position( ).get_alt( )
		)
	}
}

/// Two Drones are equal if they have the same UUID.
impl PartialEq for Drone {
	fn eq( &self, rhs: &Drone ) -> bool {
		self.uuid == rhs.uuid
	}
}

/// Interface for a drone tracker
pub trait DroneTracker {
	/// Clear all Drone entries.
	fn clear( &mut self );

	/// Returns the number of Drones currently in the DroneTracker.
	fn len( &self ) -> usize;

	/// Update will insert or update the given Drone.
	///
	/// This function does not change the last_updated time of a Drone.
	fn update( &mut self, drone: Drone );

	/// Remove a drone by UUID.
	///
	/// `remove` can safely be called multiple times for the same UUID.
	fn remove( &mut self, uuid: u32 );

	/// Return a vector of all drones.
	///
	/// This is garuanteed to be neither efficient, nor sorted at this time.
	fn list( &self ) -> Vec< Box< Drone > >;

	/// Return a vector of drones that haven't moved in `secs` seconds.
	///
	/// This will sorted by time of last movement, oldest first.
	fn get_unmoved( &self, secs: i64 ) -> Vec< Box< Drone > >;

	/// Generate drones for testing purposes.
	///
	/// This will add four Drones to the DroneTracker. Their movment times will be based upon the current time.
	/// Two will have moved recently, two will not have moved in over eight minutes.
	///
	/// Movement order, most recent to least recent, is [ 4, 2, 1, 3 ]
	fn load_example_data( &mut self ) {
		self.clear( );
		self.update( Drone {
				uuid: 1,
				state: DroneState {
					pos: Position::new( 1.0, 1.0, 1.0 ),
					last_movement: Utc::now( ) - Duration::seconds( 500 ),
					last_update: Utc::now( ),
				}
		} );
		self.update( Drone {
				uuid: 2,
				state: DroneState {
					pos: Position::new( 2.0, 2.0, 2.0 ),
					last_movement: Utc::now( ),
					last_update: Utc::now( ),
				}
		} );
		self.update( Drone {
				uuid: 3,
				state: DroneState {
					pos: Position::new( 3.0, 3.0, 3.0 ),
					last_movement: Utc::now( ) - Duration::seconds( 600 ),
					last_update: Utc::now( ),
				}
		} );
		self.update( Drone {
				uuid: 4,
				state: DroneState {
					pos: Position::new( 4.0, 4.0, 4.0 ),
					last_movement: Utc::now( ),
					last_update: Utc::now( ),
				}
		} );
	}
}

/// Memory based DroneTracker.
/// This DroneTracker implementation stores drones on the heap.
/// It is threadsafe.
pub struct DroneTrackerMem {
	data: Arc< RwLock< Vec< Box< Drone > > > >,
}

impl DroneTrackerMem {
	/// Create a new memory based DroneTracker.
	pub fn new( ) -> DroneTrackerMem {
		DroneTrackerMem{ data: Arc::new( RwLock::new( Vec::new( ) ) ) }
	}

	/// A Clone like function.
	///
	/// Creates a new object that refers to the same memory on the heap.
	// TODO: This may be semantically identical to clone, evaluate.
	pub fn duplicate( &self ) -> DroneTrackerMem {
		DroneTrackerMem{ data: self.data.clone() }
	}
}

impl DroneTracker for DroneTrackerMem {
	fn clear( &mut self ) {
		// Write lock, get the vector, and clear.
		self.data.write( ).unwrap( ).clear( );
	}

	fn len( &self ) -> usize {
		// Read lock, get the vector, and return it's length.
		self.data.read( ).unwrap( ).len( )
	}

	fn update( &mut self, drone: Drone ) {
		// Read lock, get the vector, and search for the UUID. If not found it gives the proper place to insert.
		// This maintains sorted order by UUID.
		let loc = self.data.read( ).unwrap( ).binary_search_by( | i | i.uuid.cmp( &drone.uuid ) );

		match loc {
			// Update the drone.
			Ok( i ) => self.data.write( ).unwrap( )[ i ] = Box::new( drone ),

			// insert a new drone.
			Err( i ) => self.data.write( ).unwrap( ).insert( i, Box::new( drone ) ),
		};
	}

	fn remove( &mut self, uuid: u32 ) {
		// Read lock, get the vector, and search for the UUID.
		let loc = self.data.read( ).unwrap( ).binary_search_by( | i | i.uuid.cmp( &uuid ) );

		// Get a write lock and remove if found.
		if loc.is_ok( ) {
			self.data.write( ).unwrap( ).remove( loc.unwrap( ) );
		}
	}

	fn list( &self ) -> Vec< Box< Drone > > {
		// Get a read lock, and do a quick and dirty copy.
		self.data.read( ).unwrap( ).to_vec( )
	}

	fn get_unmoved( &self, secs: i64 ) -> Vec< Box< Drone > > {
		// Get a read lock, and do a quick and dirty copy.
		// TODO: Use a filtered copy instead.
		let mut r = self.data.read( ).unwrap( ).to_vec( );

		// Here we retain only those elements that have not moved in the last `secs` seconds
		r.retain( | a | Utc::now( ) - a.get_last_movement( ) > Duration::seconds( secs ) );

		// Sort the vector by the last update time, which is the sorting order of DroneState. Stable ordering is
		// not important here, so we use the faster unstable sort.
		r.sort_unstable_by( | a, b | a.state.cmp( &b.state ) );

		// Return the sorted vector
		r
	}
}
