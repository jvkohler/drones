//! # Drone Tracker
//!
//! A server to track multiple drones in real time.
//!
//! ## Requirements
//!
//! A modern GNU/Linux environment is required to run or build this program. This has been tested on Debian 9.
//! The program binds to port 8080, so this port must be available for the program to run. There program writes
//! no data during it's execution, so should be able to be run on read only filesystems and images, but this
//! has not been tested.
//!
//! ## Security
//!
//! This program very likely needs the `CAP_BLOCK_SUSPEND` pocess capability for epoll(7), but this has not been
//! verified. This program runs happily as an unprivileged user, and you are encouraged to run it as such to
//! help safeguard against unknown vulnerabilites in the HTTP library used.
//!
//! Given that there are currently no ways to send data directly into the program, the lack of authentication
//! and authorization are not terrible things, but you are still encouraged to limit access to the HTTP server
//! to trusted networks.
//!
//! ## How to run
//!
//! Simply run the `drones` command and the server will listen on port 8080 on all available network interfaces.
//!
//! ## Configuration
//!
//! There is no configuration at this time.
//!
//! ## How to exit
//!
//! At this time, the program will run forever until killed. Typing `Ctrl + C` into the controlling terminal
//! or running `killall drones` elsewhere on the same machine will do the job.
//!

mod tracker;
mod server;

use tracker::DroneTracker;

fn main() {
	let mut t: tracker::DroneTrackerMem = tracker::DroneTrackerMem::new( );
	t.load_example_data( );
	// TODO: Spawn as background server instead.
	let s = server::start_server( "0.0.0.0", 8080, &t.duplicate( ), false );
	// TODO: wait for trigger
	s.stop( );
}
