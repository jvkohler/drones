//! # The HTTP server module for the Drone Tracker
//!
//! ## Endpoints
//!
//! ### `/` and `/drones`
//!
//! This will return a list of all Drones in the DroneTracker. There is no defined sorting order at this time.
//! There is no way to retrieve this data in offset+count form at this time.
//!
//! ### `/unmoving`
//!
//! This will return a list of all Drones in the DroneTracker that have not moved in 10 seconds. This list will be
//! sorted by time of last detected movement, ascending. In other words, oldest first, newest last.
//!
//! ## Result format
//!
//! ### JSON Structure
//!
//! All endpionts return JSON objects of with a `result` field. This field is a list of [Drones] that match the
//! query. This list may be empty if no Drones should be returned.
//!
//! The format of the Drone JSON representation is documented in the [tracker] module.
//!
//! ## Starting a Server
//!
//! Call [start_server].
//!

// Externs
extern crate hyper;

// Std lib
use std::net::{ SocketAddr, IpAddr };
use std::thread;

// Dependencies
use self::hyper::{ Body, Method, Request, Response, Server, StatusCode };
use self::hyper::rt::Future;
use self::hyper::service::service_fn_ok;

// Our modules
use tracker::{ DroneTracker, DroneTrackerMem };

#[ cfg( test ) ]
mod tests {
	// Externs
	extern crate reqwest;

	// Dependencies
	use self::reqwest::Url;
	use std::sync::{ Once, ONCE_INIT };

	// Our modules
	use server;
	use tracker::{ DroneTracker, DroneTrackerMem };

	// Convenience function for easy changes later.
	// returns: a reqwest::Url
	fn get_base_uri( ) -> self::reqwest::Url {
		let uri = Url::parse( "http://127.0.0.1:8080" ).unwrap( );

		return uri;
	}

	// Start the application server, exactly once.
	// The testing order is indeterminate, so we need to ensure that
	// each test can start a server, but only one will succeed.
	fn setup_http_server( ) {
		static START: Once = ONCE_INIT;
		let mut t = DroneTrackerMem::new( );

		// Preseed the test
		t.load_example_data( );

		// Initialize the server once
		START.call_once( | | {
			server::start_server( "127.0.0.1", 8080, &t.duplicate( ), true );
		} );
	}

	// Make a GET reqest to the given path
	// Returns the reqwest::Response object.
	fn check_http_get( path: &str ) -> reqwest::Response {
		let uri = get_base_uri( ).join( path ).unwrap( );

		reqwest::get( uri ).unwrap( )
	}

	// Make a GET reqest to the given path
	// Returns true if the HTTP status is 200, false otherwise.
	fn check_http_get_result( path: &str ) -> bool {
		check_http_get( path ).status( ).is_success( )
	}

	#[ test ]
	fn http_sanity( ) {
		// Ensure that the server is running
		setup_http_server( );

		// Make sure that a basic endpoint is available.
		// Make sure that we aren't answering all endpoints.
		assert_eq!( check_http_get_result( "/nonexistant_path" ), false );
		assert_eq!( check_http_get_result( "/" ), true );
	}

	#[ test ]
	fn http_api_endpoints_available( ) {
		// Ensure that the server is running
		setup_http_server( );

		// API Endpoint availability
		assert_eq!( check_http_get_result( "/drones" ), true );
		assert_eq!( check_http_get_result( "/unmoving" ), true );

		// Testing the endpoint currently is very difficult as the times change
		// Will need to rework the HTTP output before that can happen.
		//let mut r = check_http_get( "/drones" );
		//assert_eq!( r.text( ).unwrap( ), "" );
	}
}

/// An opaque interface for managing spawned HTTP server threads
#[ derive( Debug ) ]
pub struct ServerThread {
	join_guard: thread::JoinHandle< ( ) >,
}

impl ServerThread {
	/// Stop the thread
	///
	/// Currently unimplemented.
	pub fn stop( &self ) {
		// Here we should be sending a message to the thread to stop
		// Alternatively, we could use pthread_kill
	}
}

/// Start the web server.
/// # Arguments
///
/// `addr` A str that can be interpreted as an IPv4 or IPv6 address.
///
/// `port` The port to bind to. 1 - 65535.
///
/// `manager` A reference to the DroneTracker that you would like to be associated with the server. It will be duplicated.
///
/// `background` If `false`, this call will never return and the HTTP server will take over this thread. If `true`, then
/// a new thread will be spawned and this function will return.
///
/// # Returns
///
/// A [ServerThread] instance is returned representing the spawned thread.
///
/// # Panics
///
/// Passing a port of `0` will cause a panic. There are many network related panics that can happen, including addresses
/// that can't be interpreted, ports in use, and probably more.
pub fn start_server( addr: &str, port: u16, manager: &DroneTrackerMem, background: bool ) -> ServerThread {
	// Create a service with our manager.
	assert_ne!( port, 0 );
	let manager = manager.duplicate( );
	let service = move || {
		service_fn_ok( handle_request_builder( &manager ) )
	};

	// Create a server with our service
	let socket_addr = SocketAddr::new( addr.parse::< IpAddr >( ).unwrap( ), port );
	let server = Server::bind( &socket_addr )
		.serve( service )
		.map_err( | e | eprintln!( "Server Error: {}", e ) );

	// TODO: There's a way to get this from the server, which would be more correct.
	println!( "Listening on {}", socket_addr );

	// Run the server in the foreground or background.
	if background {
			ServerThread{ join_guard: thread::spawn( || { hyper::rt::run( server ) } ) }
	} else {
		hyper::rt::run( server );
		// FIXME: What a hack; but we need to store something.
		ServerThread{ join_guard: thread::spawn( | | { } ) }
	}
}

/// Create a new request handler with our tracker added as a parameter.
///
/// This will be called dynamically as the server spawns new threads to handle requests. This is why
/// we are duplicating the [DroneTracker]. We are copying from the main thread's service to the
/// individual handler's thread. We're essentially decorating the request handler function signature.
fn handle_request_builder( t: &DroneTrackerMem ) -> impl Fn( Request<Body > ) -> Response< Body > {
	// Copy and mask the tracker
	let t: DroneTrackerMem = t.duplicate( );

	// return the closure
	move | a | { handle_request( &a, &t ) }
}

/// Handle web requests.
///
/// All results will be returned as a JSON object with a `result` vector. The [Drones], if any, will be
/// returned as objects inside the `result` vector.
///
/// All unmapped endpoints will result in a 404 Not Found status code and accompanying HTML page.
fn handle_request( req: &Request<Body>, t: &DroneTrackerMem ) -> Response<Body> {
	let mut response = Response::new( Body::empty( ) );

	// JSON result object wrappers
	let intro: String = String::from("{ \"result\" : [\n");
	let outro: String = String::from("]}\n");

	//FIXME:: Authentication and Authorization
	//TODO:: These should dispatch to separate functions.
	match ( req.method( ), req.uri( ).path( ) ) {
		( &Method::GET, "/" ) | ( &Method::GET, "/drones" ) => {
			// TODO: Parse query parameters

			// Retrieve the list
			let mut res_strings: Vec< String > = vec![ ];
			for d in t.list( ) {
					res_strings.push( d.to_json( ) );
			}

			// Return the drones
			*response.body_mut( ) = Body::from( intro + &res_strings.join(",\n") + &outro );
		},

		// This isn't the right match, but it's an OK placeholder.
		// Return an individual Drone.
		( &Method::GET, "/drone/" ) => {
			// TODO: Retrieve the drone

			// Return the drone
			*response.body_mut( ) = Body::from( intro + &String::from("") + &outro );
		},

		// Return the Drones that haven't moved in 10 seconds.
		( &Method::GET, "/unmoving" ) => {
			// Retrieve the list of unmoving drones
			let mut res_strings: Vec< String > = vec![ ];
			for d in t.get_unmoved( 10 ) {
					res_strings.push( d.to_json( ) );
			}

			// Return the drones
			*response.body_mut( ) = Body::from( intro + &res_strings.join(",\n") + &outro );
		},

		// 404 Not found
		_ => {
			*response.body_mut( ) = Body::from( "<html><head><title>Not Found</title></head><body><h1>Page not found</h1></body></html>" );
			*response.status_mut( ) = StatusCode::NOT_FOUND;
		}
	};
	response
}
