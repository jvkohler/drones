Drone tracker
=============

A server application that will report the locations of drones.

Assumptions
-----------

We assume that the drone tracker will handle many concurrent drones, on the order of hundreds of thousands to millions.
Administration of this drone fleet should be very simple, consequently we will not require pre-registration of the drones.
In the absence of authentication and authorization layers, we presume that this is being done at proxy level for the web interface,
or on the communication channel for the drones. 

While we don't want pre-registration, we do need to map each drone to a UUID. This can be accomplished automaticaaly by using the
hardware address of the drone's cellular modem as the UUID, which would require a new UUID implementation internally but is
something this project will support in the future. The hardware address could be a MAC address, if stably assigned to the device,
an MEI or IMEI, or any other unique hardware identifier.

There is a current limitation of 4,294,967,295 drone UUIDs, as the internal implementation of UUID is an unsigned 32 bit integer.
In the future this could be reimplemented as a hardware identifier.

We presume that a decimal accuracy of six places is sufficient for the geospatial positions of the drones. This is an accuracy of
between 111mm and 43mm depending upon location[^1][1]

[1]: https://en.wikipedia.org/wiki/Decimal_degrees

Drone Location Protocol
-----------------------

The drones are communicating to us over a cellular modem, without packet loss or interruption, once a second. To minimize the cost
we send the smallest packets we can. For now we will presume a UUID based upon the MAC address of the modem, as it's one of the largest
likely UUID representations. 

All multi-octet data representations are in network byte order with bits in each octet in network bit order. Both are big endian.

### Packet Format

#### Packet, 152 bits or 19 octets

| Bits    | Size      | Purpose                          |
|---------|-----------|----------------------------------|
| 1..56   | 7 octets  | Packet Header, specified herein. |
| 57..152 | 12 octets | Data, specified herein.          |

#### Packet header, 56 bits or 7 octets

| Bits   | Size     | Purpose                                                                             |
|--------|----------|-------------------------------------------------------------------------------------|
| 1..8   | 1 octet  | Packet version or format. This documents version 1. Big endian byte representation. |
| 9..56  | 6 octets | UUID. MAC address. Six individual octets, most significant first.                   |


#### Packet Data, 96 bits or 12 octets

| Bits   | Size     | Purpose                                                                                    |
|--------|----------|--------------------------------------------------------------------------------------------|
| 1..32  | 4 octets | Latitude as a single precision floating point number, IEEE 754 binary32 representation     |
| 33..64 | 4 octets | Longitude as a single precision floating point number, IEEE 754 binary32 representation    |
| 65..96 | 4 octets | Altitude as a single precision floating point number, IEEE 754 binary32 representation     |

Maximum monthly data consumption per drone would be:

    19 Bytes per second * 60 secs/min * 60 mins/hr * 24 hrs/day * 31 day/mo = 50,889,600 Bytes

Or, approximately 49 MB a month rounded up.

Transport and network layer packetization will increase that dramatically. If this were delivered over IPv6/UDP it would add 48 Bytes
per request for packet framing. This would more than triple the cost to around 172 MB per month. Framing for the link level communication
over the cellular modem will add even more size to the packet.

Given our packet's size related to the delivery information's size, there is not much point in adding processing complexity through
compression, ultra-compact data representations, or delta-state reporting. Even enlarging the data packet a small amount will have a
negligible effect upon total data consumption.

Configuration
-------------

There is none available at this time. This will change in the future.

How to Use
----------

There is a Makefile with basic commands for Cargo and Docker. These are here to save typing and configuration.

How to build a docker image:

    make docker-build

How to run as a docker image:

    make docker-run

or just

    make

Once you're running the container you can connect to it on port 8080.

Developer Commands
------------------

None of the following commands use or interact with docker or a running docker image.

How to error check the source code without building an executable:

    make check

How to run development tests, no docker. These will require extra dependencies, see the requirements section below.

    make test

How to run the linter, no docker.

    make lint

How to compile the source code, no docker:

    make build

Run the program locally, no docker.

    make run

Generate the documentation

    make docs

### Developer Requirements

You will need to have libssl development packages installed to run the tests. You may also need the pkg-config utility. On Debian derived distributions this do this with:

    sudo apt-get install libssl-dev pkg-config

Additionally, to use the clippy linter you need to have a nightly build of rustc installed. 

### HTTP Endpoints

Path       | Method | Function
-----------|--------|-----------------------------------------------
/          | GET    | List all drones.
/drones    | GET    | List all drones.
/unmoved   | GET    | List all drones that haven't moved recently.
